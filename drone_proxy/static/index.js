function log(obj){
	document.getElementById("log").value = obj;
}

var hover = 0.0;
var rotate = 0.0;
var pan = [0.0, 0.0];

function repaint_state(state) {
	for (var i=0; i<32; i++) {
		var sw = document.getElementById("s"+i);
		if ((state & (1<<i)) != 0) {
			sw.className = "on";
		} else {
			sw.className = "off";
		}
	}
}

var control;
function control_setup(){
	control = new WebSocket("ws://"+document.getElementById("host").value+"/control");
	control.onopen = function(){
		repaint_state(0);
	}
	control.onclose = function(){
		repaint_state(0);
		setTimeout("control_setup()", 2000);
	}
	control.onerror = function(ev) {
		console.log(ev);
	}
	control.onmessage = function(ev) {
		var msg = JSON.parse(ev.data)
		if (msg.result.Type == "state") {
			repaint_state(msg.result.State);
		} else if (msg.result.Type == "demo") {
			var msg = "theta=" + msg.result.Theta;
			msg += " phi=" + msg.result.Phi;
			msg += " psi=" + msg.result.Psi;
			msg += " alt=" + msg.result.Altitude;
			document.getElementById("demo").innerHTML = msg
		} else {
			console.log(msg);
		}
	}
}

function pcmd_send() {
	var cmd = JSON.stringify({
		"method": "pcmd_mag",
		"params": [ 5, pan[0], pan[1], hover, rotate/480.0, 0.0, 0.0 ],
		"id": new Date().toString(),
	});
	console.log(cmd);
	try {
		control.send(cmd);
	} catch (e) {
		console.log(e);
	}
}

function reset_gaz(){
	hover = 0.0;
	pcmd_send();
}

function takeoff(ev){
	var req = new XMLHttpRequest();
	req.open("GET","http://"+document.getElementById("host").value+"/takeoff", true);
	req.send();
	console.log("takeoff");
}

function magneto(ev){
	var req = new XMLHttpRequest();
	req.open("GET","http://"+document.getElementById("host").value+"/magneto", true);
	req.send();
	console.log("magneto");
}

var pinch_sleep = false;
function pinch_awake(){
	hover = 0.0;
	pcmd_send();
	pinch_sleep = false;
}

var rotate_sleep = false;
function rotate_awake(){
	rotate = 0.0;
	pcmd_send();
	rotate_sleep = false;
}

var pan_sleep = false;
function pan_awake(){
	pan = [0.0, 0.0];
	pcmd_send();
	pan_sleep = false;
}

window.onload = function() {
	var pad = document.getElementById("pad");
	var h = new Hammer(pad);
	h.get("pan").set({direction:Hammer.DIRECTION_ALL});
	h.get("pinch").set({enable:true});
	h.get("rotate").set({enable:true});
	
	h.on("pinchstart", function(ev){
		prev_scale = ev.scale;
	});
	h.on("pinch", function(ev){
		if (!pinch_sleep) {
			document.getElementById("scale").value = ev.scale/prev_scale;
			hover = -Math.log(ev.scale/prev_scale)*5.0;
			pcmd_send();
			prev_scale = ev.scale;
			pinch_sleep = true;
			setTimeout("pinch_awake()", 100);
		}
	});
	
	h.on("doubletap", takeoff);
	
	var prev_rotation = 0.0;
	h.on("rotatestart", function(ev){
		prev_rotation = ev.rotation;
	});
	h.on("rotate", function(ev){
		if (!rotate_sleep) {
			document.getElementById("rotate").value = ev.rotation - prev_rotation;
			rotate = (prev_rotation - ev.rotation + 360.0) % 360 - 180.0;
			pcmd_send();
			prev_rotation = ev.rotation;
			rotate_sleep = true
			setTimeout("rotate_awake()", 100);
		}
	});
	
	var prev_pan = [0,0];
	h.on("panstart", function(ev){
		prev_pan = [ev.deltaX, ev.deltaY];
	});
	h.on("panmove", function(ev) {
		if (!pan_sleep) {
			// ev.target max(width, height)
			pan = [ev.deltaX/400.0, ev.deltaY/400.0];
			pcmd_send();
			prev_pan = pan;
			pan_sleep = true
			setTimeout("pan_awake()", 100);
		}
	});
	
	document.getElementById("takeoff").onclick = takeoff;
	document.getElementById("magneto").onclick = magneto;
	control_setup();
}
