function log(msg) {
	document.getElementById("log").value = msg + "\n" + document.getElementById("log").value
}

var pcmd;
function pcmd_setup(){
	pcmd = new WebSocket("ws://"+location.host+"/pcmd");
	pcmd,onopen = function(){
		document.getElementById("pcmd").innerText = "pcmd=connected";
	}
	pcmd.onclose = function(){
		document.getElementById("pcmd").innerText = "pcmd=disconnected";
		setTimeout("pcmd_setup()", 2);
	}
}

function neutral(){
	var cmd = JSON.stringify({
		"Flag": 1,
		"Roll": 0.0,
		"Pitch": 0.0,
		"Gaz": 0.0,
		"Yaw": 0.0,
	});
	log(cmd);
	pcmd.send(cmd);
}

var pressed = false;
var startX, startY;
var bgX=0, bgY=0;
var prevX, prevY

function handleMouse(ev){
	switch(ev.type){
	case "mousedown":
		startX = ev.clientX;
		startY = ev.clientY;
		pressed = true;
		break;
	case "mousemove":
		if (pressed) {
			var x = bgX + ev.clientX - startX;
			var y = bgY + ev.clientY - startY;
			ev.target.style.backgroundPositionX = x+"px";
			ev.target.style.backgroundPositionY = y+"px";
			var cmd = JSON.stringify({
				"Flag": 1,
				"Roll": (ev.clientX - prevX)/20.0,
				"Pitch": (ev.clientY - prevY)/20.0,
				"Gaz": 0.0,
				"Yaw": 0.0,
			});
			log(cmd);
//			setTimeout("neutral()", 200); // Due to drone environment air flow, neutral does not work as expected.
			pcmd.send(cmd);
		}
		break;
	default:
		pressed = false;
		bgX = bgX + ev.clientX - startX;
		bgY = bgY + ev.clientY - startY;
		break;
	}
	prevX = ev.clientX;
	prevY = ev.clientY;
}

window.onload = function() {
	document.getElementById("takeoff").onclick = function(){
		var req = new XMLHttpRequest();
		req.open("GET","/takeoff", true);
		req.send();
	}
	
	var pad = document.getElementById("pad");
	pad.addEventListener("mousedown", handleMouse, false);
	pad.addEventListener("mouseup", handleMouse, false);
	pad.addEventListener("mousemove", handleMouse, false);
	
	pcmd_setup();
}
