function simpleRequest(url){
	var req = new XMLHttpRequest();
	req.open("GET", url, true)
	req.onload = function(e){
		if (req.readyState==4){
			document.getElementById("log").innerHTML = req.responseText;
		}
	}
	req.onerror = function(e){
		document.getElementById("log").innerHTML = req.statusText;
	}
	req.send(null);
}

addEventListener("load", function(){
	document.getElementById("loglines").onclick = function(){
		simpleRequest("http://127.0.0.1:8080/loglines");
	};
	document.getElementById("connect").onclick = function(){
		simpleRequest("http://127.0.0.1:8080/connect");
	};
	document.getElementById("disconnect").onclick = function(){
		simpleRequest("http://127.0.0.1:8080/disconnect");
	};
});
