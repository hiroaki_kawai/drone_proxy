package main

/*

To build for arm platform, such as Open web board, run as following:
    GOARCH=arm go build -o ard_arm ard.go
    adb push ard_arm /path/to/writable_dir

*/

import (
	"fmt"
	"flag"
	"log"
	"net"
	"net/http"
	"errors"
	"bytes"
	"time"
	"encoding/binary"
	"sync"
	"math"
	"encoding/json"
	"golang.org/x/net/websocket"
	_ "net/http/pprof"
)

const (
	ARDRONE_FLY_MASK = 1<<iota
	ARDRONE_VIDEO_MASK
	ARDRONE_VISION_MASK
	ARDRONE_CONTROL_MASK
	ARDRONE_ALTITUDE_MASK
	ARDRONE_USER_FEEDBACK_START
	ARDRONE_COMMAND_MASK
	ARDRONE_CAMERA_MASK
	ARDRONE_TRAVELLING_MASK
	ARDRONE_USB_MASK
	ARDRONE_NAVDATA_DEMO_MASK
	ARDRONE_NAVDATA_BOOTSTRAP
	ARDRONE_MOTORS_MASK
	ARDRONE_COM_LOST_MASK
	ARDRONE_SOFTWARE_FAULT
	ARDRONE_VBAT_LOW
	ARDRONE_USER_EL
	ARDRONE_TIMER_ELAPSED
	ARDRONE_MAGNETO_NEEDS_CALIB
	ARDRONE_ANGLES_OUT_OF_RANGE
	ARDRONE_WIND_MASK
	ARDRONE_ULTRASOUND_MASK
	ARDRONE_CUTOUT_MASK
	ARDRONE_PIC_VERSION_MASK
	ARDRONE_ATCODEC_THREAD_ON
	ARDRONE_NAVDATA_THREAD_ON
	ARDRONE_VIDEO_THREAD_ON
	ARDRONE_ACQ_THREAD_ON
	ARDRONE_CTRL_WATCHDOG_MASK
	ARDRONE_ADC_WATCHDOG_MASK
	ARDRONE_COM_WATCHDOG_MASK
	ARDRONE_EMERGENCY_MASK
)

const (
	NAVDATA_DEMO_TAG = iota
	NAVDATA_TIME_TAG
	NAVDATA_RAW_MEASURES_TAG
	NAVDATA_PHYS_MEASURES_TAG
	NAVDATA_GYROS_OFFSETS_TAG
	NAVDATA_EULER_ANGLES_TAG
	NAVDATA_REFERENCES_TAG
	NAVDATA_TRIMS_TAG
	NAVDATA_RC_REFERENCES_TAG
	NAVDATA_PWM_TAG
	NAVDATA_ALTITUDE_TAG
	NAVDATA_VISION_RAW_TAG
	NAVDATA_VISION_OF_TAG
	NAVDATA_VISION_TAG
	NAVDATA_VISION_PERF_TAG
	NAVDATA_TRACKERS_SEND_TAG
	NAVDATA_VISION_DETECT_TAG
	NAVDATA_WATCHDOG_TAG
	NAVDATA_ADC_DATA_FRAME_TAG
	NAVDATA_VIDEO_STREAM_TAG
	NAVDATA_GAMES_TAG
	NAVDATA_PRESSURE_RAW_TAG
	NAVDATA_MAGNETO_TAG
	NAVDATA_WIND_TAG
	NAVDATA_KALMAN_PRESSURE_TAG
	NAVDATA_HDVIDEO_STREAM_TAG
	NAVDATA_WIFI_TAG
	NAVDATA_ZIMMU_3000_TAG
	NAVDATA_CKS_TAG = 0xffff
)

type Navdata []byte

func (self Navdata) State() uint32 {
	return binary.LittleEndian.Uint32(self[4:])
}

func (self Navdata) Seq() uint32 {
	return binary.LittleEndian.Uint32(self[8:])
}

func (self Navdata) Vision() uint32 {
	return binary.LittleEndian.Uint32(self[12:])
}

func (self Navdata) Options() NavdataOption {
	return NavdataOption(self[16:])
}

func (self Navdata) GoString() string {
	var opts []string
	for _,opt := range NavdataOption(self[16:]).Iter() {
		opts = append(opts, fmt.Sprintf("Option(id=%x,size=%d)", opt.Id(), opt.Size()))
	}
	return fmt.Sprintf("state=%08x,seq=%d,%v", self.State(), self.Seq(), opts)
}


type NavdataOption []byte

func (self NavdataOption) Id() uint16 {
	return binary.LittleEndian.Uint16(self)
}

// Length of option total length, including id, size.
func (self NavdataOption) Size() int {
	return int(binary.LittleEndian.Uint16(self[2:]))
}

func (self NavdataOption) Data() []byte {
	return self[4:self.Size()]
}

func (self NavdataOption) Iter() []NavdataOption {
	var ret []NavdataOption
	for scan:=self; len(scan)>4; scan=scan[scan.Size():] {
		ret = append(ret, scan[:scan.Size()])
	}
	return ret
}


type Vector31 []byte

func (self Vector31) V() [3]float32 {
	return [3]float32{ self.X(), self.Y(), self.Z() }
}

func (self Vector31) X() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self))
}

func (self Vector31) Y() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[4:]))
}

func (self Vector31) Z() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[8:]))
}


type NavdataDemo []byte

func (self NavdataDemo) CtrlState() uint32 {
	return binary.LittleEndian.Uint32(self)
}

func (self NavdataDemo) VbatFlyingPercentage() uint32 {
	return binary.LittleEndian.Uint32(self[4:])
}

func (self NavdataDemo) Theta() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[8:]))
}

func (self NavdataDemo) Phi() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[12:]))
}

func (self NavdataDemo) Psi() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[16:]))
}

func (self NavdataDemo) Altitude() int32 {
	return int32(binary.LittleEndian.Uint32(self[20:]))
}

func (self NavdataDemo) Vx() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[24:]))
}

func (self NavdataDemo) Vy() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[28:]))
}

func (self NavdataDemo) Vz() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[32:]))
}



type NavdataMagneto []byte

func (self NavdataMagneto) Mx() int16 {
	return int16(binary.LittleEndian.Uint16(self))
}

func (self NavdataMagneto) My() int16 {
	return int16(binary.LittleEndian.Uint16(self[2:]))
}

func (self NavdataMagneto) Mz() int16 {
	return int16(binary.LittleEndian.Uint16(self[4:]))
}

func (self NavdataMagneto) MagnetoRaw() Vector31 {
	return Vector31(self[8:32])
}

func (self NavdataMagneto) MagnetoRectified() Vector31 {
	return Vector31(self[32:56])
}

func (self NavdataMagneto) MagnetoOffset() Vector31 {
	return Vector31(self[56:80])
}

func (self NavdataMagneto) HeadingUnwrapped() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[60:]))
}

func (self NavdataMagneto) HeadingGyroUnwrapped() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[64:]))
}

func (self NavdataMagneto) HeadingFusionUnwrapped() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[68:]))
}

func (self NavdataMagneto) MagnetoCalibrationOk() bool {
	return self[72] == 0
}

func (self NavdataMagneto) MagnetoState() uint32 {
	return binary.LittleEndian.Uint32(self[73:])
}

func (self NavdataMagneto) MagnetoRadius() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[77:]))
}

func (self NavdataMagneto) ErrorMean() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[81:]))
}

func (self NavdataMagneto) ErrorVar() float32 {
	return math.Float32frombits(binary.LittleEndian.Uint32(self[85:]))
}


const (
	NAVDATA_DST = "192.168.1.1:5554"
	NAVDATA_SRC = "0.0.0.0:5554"
	AT_DST = "192.168.1.1:5556"
	AT_SRC = "0.0.0.0:5556"
)

const(
	ARD_STATE_IDLE = iota
)

type Connection struct {
	nav *net.UDPConn
	atc *net.UDPConn
	navcond *sync.Cond
	seq uint32
	seqlock *sync.Mutex
	reconnect bool
	closed bool
	handlers []func(Navdata)
	
	State uint32
	Vision uint32
	Options map[uint16][]byte
}

func Dial() (*Connection,error) {
	self := &Connection {
		Options: make(map[uint16][]byte),
		navcond: sync.NewCond(&sync.Mutex{}),
		seq: 0,
		seqlock: &sync.Mutex{},
		reconnect: true,
	}
	
	comWdg := func(nav Navdata){
		if nav.State() & ARDRONE_COM_WATCHDOG_MASK != 0 {
			self.Command("COMWDG","")
		}
	}
	self.handlers = append(self.handlers, comWdg)
	
	makeConnection := func(local,remote string) (*net.UDPConn,error) {
		if laddr,err:=net.ResolveUDPAddr("udp", local); err!=nil {
			return nil,err
		} else if raddr,err:=net.ResolveUDPAddr("udp", remote); err!=nil {
			return nil,err
		} else if con,err:=net.DialUDP("udp", laddr, raddr); err!=nil {
			return nil,err
		} else {
			return con,nil
		}
	}
	
	go func(){
		for self.reconnect {
			if nav,err:=makeConnection(NAVDATA_SRC,NAVDATA_DST); err!=nil {
				log.Print(err)
				time.Sleep(2.0 * time.Second)
				continue
			} else if atc,err:=makeConnection(AT_SRC,AT_DST); err!=nil {
				nav.Close()
				log.Print(err)
				time.Sleep(2.0 * time.Second)
				continue
			} else {
				self.nav = nav
				self.atc = atc
			}
			
			reader := make(chan bool)
			go func() {
				buf := make([]byte, 2048)
				for !self.closed {
					if err:=self.nav.SetDeadline(time.Now().Add(5 * time.Second)); err!=nil {
						log.Print(err)
						break
					} else if n,err:=self.nav.Read(buf); err!=nil {
						log.Print(err)
						break
					} else if n < 16 {
						log.Print("navdata less than fixed length")
					} else if ! bytes.Equal(buf[:4], []byte{ 0x88, 0x77, 0x66, 0x55 }) {
						log.Print("navdata header marker error", buf[:n])
					} else {
						nav := Navdata(buf[:n])
						log.Printf("%#v", nav)
						if state:=nav.State(); self.State != state {
							log.Printf("state=%08x", state)
							self.State = state
						}
						self.navcond.Broadcast()
						// XXX: should check for self.Seq() for out-of-order UDP packets
						self.Vision = nav.Vision()
						for _,opt := range nav.Options().Iter() {
							self.Options[opt.Id()] = opt.Data()
						}
						for _,handle := range self.handlers {
							handle(nav)
						}
					}
				}
				if !self.closed {
					self.closed = true
				}
				reader <- true
			}()
			
			func(){
				self.seq = 0
				if n,err:=self.nav.Write([]byte{ 1,0,0,0 }); err!=nil {
					log.Print(err)
					return
				} else if n!=4 {
					log.Print("NAVDATA initiation failed")
					return
				}
				loop := func(cond func()bool) error {
					timeout := make(chan bool)
					go func(){
						time.Sleep(2 * time.Second)
						close(timeout)
					}()
					for {
						if cond() {
							break
						}
						ack := make(chan bool)
						go func(){
							self.navcond.L.Lock()
							self.navcond.Wait()
							self.navcond.L.Unlock()
							close(ack)
						}()
						select {
						case <-timeout:
							return errors.New("loop timeout")
						case <-ack:
							// new message arrived.
						}
					}
					return nil
				}
				var skip bool = true
				if err:=loop(func()bool{
					if skip {
						skip = false
						return false
					} else {
						return true
					}
				}); err!=nil {
					log.Print(err)
					return
				}
				if self.State & ARDRONE_NAVDATA_BOOTSTRAP != 0 {
					self.Command("CONFIG","\"general:navdata_demo\",\"TRUE\"")
					if err:=loop(func()bool{
						if self.State & ARDRONE_COMMAND_MASK != 0 {
							self.Command("CTRL","0")
							return true
						} else {
							return false
						}
					}); err!=nil {
						log.Print(err)
						return
					}
				}
				for {
					if self.State & ARDRONE_COM_LOST_MASK != 0 {
						return
					}
					timeout := make(chan bool)
					go func() {
						time.Sleep(10*time.Second)
						close(timeout)
					}()
					ack := make(chan bool)
					go func(){
						self.navcond.L.Lock()
						self.navcond.Wait()
						self.navcond.L.Unlock()
						close(ack)
					}()
					select {
					case <-ack:
						// nothing to do.
					case <-timeout:
						return
					}
				}
			}()
			if self.closed != true {
				self.closed = true
			}
			self.nav.Close()
			self.atc.Close()
			_ = <-reader
			time.Sleep(2 * time.Second)
			
			log.Print("reconnecting")
			self.State = 0
			self.closed = false
		}
		log.Print("reconnect canceled")
	}()
	return self,nil
}

func (self *Connection) Command(name, args string) error {
	self.seqlock.Lock()
	defer self.seqlock.Unlock()
	
	self.seq++
	var data string
	if len(args) > 0 {
		data = fmt.Sprintf("AT*%s=%d,%s\r", name, self.seq, args)
	} else {
		data = fmt.Sprintf("AT*%s=%d\r", name, self.seq)
	}
	log.Print(data)
	if n,err:=self.atc.Write([]byte(data)); err!=nil {
		return err
	} else if n<len(data) {
		return errors.New("at command send error")
	}
	return nil
}

type RpcRequest struct {
	Method string
	Params json.RawMessage
	Id string
}

type RpcNotification struct {
	Result interface{} `json:"result"`
}

type RpcResponse struct {
	Result interface{} `json:"result"`
	Id string `json:"id"`
}

type RpcError struct {
	Error ErrorObj `json:"error"`
	Id string `json:"id"`
}

type ErrorObj struct {
	Code int `json:"code"`
	Message string `json:"message"`
	Data interface{} `json:"data"`
}

func (self ErrorObj) Error() string {
	return self.Message
}

type StateChanged struct {
	Type string // "state"
	State uint32
}

type DemoChanged struct {
	Type string // "demo"
	Theta float32
	Phi float32
	Psi float32
	Altitude int
}

var useRingLog *bool = flag.Bool("ringlog", false, "use ringlog")

func main() {
	flag.Parse()
	
	ring := &RingLogger{}
	if *useRingLog {
		log.SetOutput(ring)
	}
	
	var ard *Connection = nil
	ardLock := &sync.Mutex{}
	connectArd := func(){
		if con,err:=Dial(); err!=nil {
			log.Print(err)
		} else {
			log.Print("CONNECT OK")
			ard = con
		}
	}
	
	http.HandleFunc("/loglines", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(ring.Dump()))
	})
	http.HandleFunc("/connect", func(w http.ResponseWriter, r *http.Request) {
		ardLock.Lock()
		defer ardLock.Unlock()
		if ard == nil || ard.reconnect == false{
			connectArd();
		} else {
			log.Print("CONNECT CONNECTED")
		}
		w.Write([]byte(ring.Dump()))
	})
	http.HandleFunc("/disconnect", func(w http.ResponseWriter, r *http.Request) {
		ardLock.Lock()
		defer ardLock.Unlock()
		if ard.reconnect {
			ard.reconnect = false
			ard.closed = true
			log.Print("DISCONNECT OK")
		} else {
			log.Print("DISCONNECT NOCONNECTION")
		}
		w.Write([]byte(ring.Dump()))
	})
	http.HandleFunc("/takeoff", func(w http.ResponseWriter, r *http.Request) {
		if ard.State & ARDRONE_FLY_MASK != 0 {
			if err:=ard.Command("REF", fmt.Sprintf("%d", 0x0000)); err!=nil {
				fmt.Fprintf(w, "%v", err)
			} else {
				fmt.Fprintf(w, "FLY->LAND")
			}
		} else {
			if err:=ard.Command("FTRIM", ""); err!=nil {
				fmt.Fprintf(w, "%v", err)
			} else if err:=ard.Command("REF", fmt.Sprintf("%d", 0x0200)); err!=nil {
				fmt.Fprintf(w, "%v", err)
			} else {
				fmt.Fprintf(w, "LAND->FLY")
			}
		}
	})
	http.HandleFunc("/magneto", func(w http.ResponseWriter, r *http.Request) {
		if err:=ard.Command("CALIB", "0"); err!=nil {
			fmt.Fprintf(w, "%v", err)
		} else {
			fmt.Fprintf(w, "OK")
		}
	})
	http.Handle("/control", websocket.Handler(
		func(ws *websocket.Conn) {
			if ard == nil {
				connectArd();
			}
			control(ws, ard)
		}))
	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/static/index.html", 301)
	})
	http.ListenAndServe(":8080",nil)
}

func control(ws *websocket.Conn, ard *Connection) {
	closed := false
	enc := json.NewEncoder(ws)
	dec := json.NewDecoder(ws)
	go func(){
		var prev_state uint32 = 0
		var prev_demo []byte
		for !closed {
			state := ard.State &^ ARDRONE_COM_WATCHDOG_MASK
			if state != prev_state {
				enc.Encode(RpcNotification{
					Result: StateChanged {
						Type: "state",
						State: state,
					},
				})
				prev_state = state
			}
			if data,ok:=ard.Options[NAVDATA_DEMO_TAG]; ok {
				demo := NavdataDemo(data)
				if len(prev_demo) > 24 && ! bytes.Equal(demo[8:24], prev_demo[8:24]) {
					enc.Encode(RpcNotification{
						Result: DemoChanged {
							Type: "demo",
							Theta: demo.Theta(),
							Phi: demo.Phi(),
							Psi: demo.Psi(),
							Altitude: int(demo.Altitude()),
						},
					})
				}
				prev_demo = demo
			}
			
			func(){
				ard.navcond.L.Lock()
				ard.navcond.Wait()
				ard.navcond.L.Unlock()
			}()
		}
	}()
	for {
		var req RpcRequest
		if err:=dec.Decode(&req); err!=nil {
			break
		} else {
			switch req.Method {
			default:
				enc.Encode(RpcError{
					Error: ErrorObj {
						Code: -32601,
						Message: "Method not found",
					},
					Id: req.Id,
				})
			case "pcmd", "PCMD": // Progressive CoMmanD message
				if resp,err:=pcmd(req, ard); err!=nil {
					enc.Encode(RpcError{
						Error: err.(ErrorObj),
						Id: req.Id,
					})
				} else {
					enc.Encode(RpcResponse{
						Result: resp,
						Id: req.Id,
					})
				}
			case "pcmd_mag", "PCMD_MAG":
				if resp,err:=pcmd_mag(req, ard); err!=nil {
					enc.Encode(RpcError{
						Error: err.(ErrorObj),
						Id: req.Id,
					})
				} else {
					enc.Encode(RpcResponse{
						Result: resp,
						Id: req.Id,
					})
				}
			}
		}
	}
	closed = true
}


func pcmd(req RpcRequest, ard *Connection) (*RpcResponse, error) {
	var params interface{}
	args := make([]float32, 5)
	params = &args
	if err:=json.Unmarshal(req.Params, params); err!= nil {
		return nil, ErrorObj {
			Code: -32602,
			Message: "Invalid params",
		}
	} else {
		for i:=1; i<5; i++ {
			if args[i] > 1.0 {
				args[i] = 1.0
			} else if args[i] < -1.0 {
				args[i] = -1.0
			}
		}
		if err:=ard.Command("PCMD", fmt.Sprintf("%d,%d,%d,%d,%d",
			int32(args[0]), // Flag
			int32(math.Float32bits(args[1])), // Roll
			int32(math.Float32bits(args[2])), // Pitch
			int32(math.Float32bits(args[3])), // Gaz
			int32(math.Float32bits(args[4])))); err!=nil { // Yaw
			return nil, ErrorObj {
				Code: -32603,
				Message: "AT* failed",
			}
		} else {
			return nil, nil
		}
	}
}

func pcmd_mag(req RpcRequest, ard *Connection) (*RpcResponse, error) {
	var params interface{}
	args := make([]float32, 7)
	params = &args
	if err:=json.Unmarshal(req.Params, params); err!= nil {
		return nil, ErrorObj {
			Code: -32602,
			Message: "Invalid params",
		}
	} else {
		for i:=1; i<7; i++ {
			if args[i] > 1.0 {
				args[i] = 1.0
			} else if args[i] < -1.0 {
				args[i] = -1.0
			}
		}
		if err:=ard.Command("PCMD_MAG", fmt.Sprintf("%d,%d,%d,%d,%d,%d,%d",
			int32(args[0]), // Flag
			int32(math.Float32bits(args[1])), // Roll
			int32(math.Float32bits(args[2])), // Pitch
			int32(math.Float32bits(args[3])), // Gaz
			int32(math.Float32bits(args[4])), // Yaw
			int32(math.Float32bits(args[5])), // Psi
			int32(math.Float32bits(args[6])))); err!=nil { // PsiAccuracy
			return nil, ErrorObj {
				Code: -32603,
				Message: "AT* failed",
			}
		} else {
			return nil, nil
		}
	}
}

const RING_SIZE = 64

type RingLogger struct{
	Buf [RING_SIZE]string
	Index int
}

func (self *RingLogger) Write(p []byte) (n int, err error) {
	self.Buf[self.Index%RING_SIZE] = string(p)
	self.Index++
	return len(p), nil
}

func (self *RingLogger) Dump() string {
	var ret []byte
	for i:=0; i<RING_SIZE; i++ {
		content := self.Buf[i%RING_SIZE]
		if len(content) > 0 {
			line := append([]byte(content), []byte("<br/>")...)
			ret = append(line, ret...)
		}
	}
	return string(ret)
}
