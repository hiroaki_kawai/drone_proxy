ABOUT
-----
This is [ARDrone 2.0](http://ardrone2.parrot.com/) web proxy daemon and controller web, 
developed to demonstrate drone flight with [Firefox OS](https://developer.mozilla.org/en-US/Firefox_OS) 
as a controller at the au KDDI's [fx0](http://au-fx.kddi.com/products/) launch event.

Thanks to [hammer.js](http://hammerjs.github.io/), which is licensed by Jorik Tangelder 
under MIT License.

Setup
-----
Power on ARDrone, and associate phone with that ardrone access point.

```
## Build ard.go ARM binary.
$ GOOS=linux GOARCH=arm go build -o ard_arm ard.go
## send it into phone /data
$ adb push drone_proxy /data
## start daemon
$ cd /data/drone_proxy && ./ard_arm
```

Open `http://localhost:8080/` with browser on the phone. Make sure the phone itself should be 
directed to north, because ardrone will be navigated with magnetometer.

Large gray area is the controller pad during the flight.

* swiping makes the drone move.
* pinch is up-down.
* with two fingers, you can rotate the drone.


LICENSE
-------
The MIT License (MIT)

Copyright (c) <2014> <Hiroaki KAWAI>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
